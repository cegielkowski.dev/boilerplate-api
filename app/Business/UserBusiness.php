<?php
namespace App\Business;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Cache;
use App\Libraries\Helper;

class UserBusiness {
    public function create(String $email, String $password, String $name, $image = null)
    {
        $content['message'] = 'Erro interno, contate o suporte';
        $status = 400;

        try {
            $content['message'] = 'Usuário já existe';
            $password = Hash::make($password);
            $userRepository = new UserRepository;
            $image = Helper::uploadImage($image);
            if ($this->get($email)->getStatusCode() == 404) {
                $key = "getuser_";
                Cache::forget($key);
                $userRepository->save($email, $password, $name, $image);
                $content = null;
                $status = 201;
            }
        } catch (Exception $e) {
            Log::error($e->getMessage());
        }

        return response()->json($content, $status);
    }

    public function update(String $user, String $email = null, String $password = null, String $name = null, $photo = null)
    {
        $content['message'] = 'Erro interno, contate o suporte';
        $status = 400;

        try {
            $content['message'] = 'Usuário não existe';
            $status = 404;
            $userRepository = new UserRepository;

            if ($this->get($user)->getStatusCode() == 200) {
                $updatePhoto = $updatePassword = null;
                $key = "getuser_";
                Cache::forget($key);
                Cache::forget($key . $user);

                if (!empty($password)) {
                    $updatePassword = Hash::make($password);
                }   

                if (!empty($photo)) {
                    $updatePhoto = Helper::uploadImage($photo);
                }
                $userRepository->save($email,
                    $updatePassword, 
                    $name, 
                    $updatePhoto, 
                    $user);
                $content = null;
                $status = 200;
            }
        } catch (Exception $e) {
            Log::error($e->getMessage());
        }

        return response()->json($content, $status);
    }

    public function get(String $email = null)
    {
        $content['message'] = 'Erro interno, contate o suporte';
        $status = 400;
        try {
            $content['message'] = 'Usuário não existente';
            $status = 404;
            $key = "getuser_" . $email ;
            $user = Cache::remember($key, 3600, function () use($email) {
                $userRepository = new UserRepository;
                return $userRepository->get($email);
            });
            if(!empty($user)) {
                $content = $user;
                $status = 200;
            }
        } catch (Exception $e) {
            Log::error($e->getMessage());
        }

        return response()->json($content, $status);
    }

    public function delete(String $email)
    {
        $content['message'] = 'Erro interno, contate o suporte';
        $status = 400;

        try {
            $content['message'] = 'Usuário não existe';
            $status = 404;

            if ($this->get($email)->getStatusCode() == 200) {
                $key = "getuser_";
                Cache::forget($key);
                Cache::forget($key . $email);
                $userRepository = new UserRepository;
                $userRepository->delete($email);
                $content = null;
                $status = 200;
            }
        } catch (Exception $e) {
            Log::error($e->getMessage());
        }

        return response()->json($content, $status);
    }
}
