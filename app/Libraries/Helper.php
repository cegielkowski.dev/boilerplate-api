<?php

namespace App\Libraries;

use App\Models\Comments;
use App\Models\Notifications;
use App\Models\Posts;
use App\Models\Transactions;
use App\Models\User;
use Illuminate\Support\Facades\Mail;

class Helper
{
    public static function uploadImage($image) {
        if (empty($image)) {
            return null;
        }
        $path = base_path() . '/public/uploads/images/';
        $image->move($path, $image->getClientOriginalName());
        return $image->getClientOriginalName();
     }

}
