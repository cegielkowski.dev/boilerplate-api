<?php
namespace App\Repositories;
use App\Models\User;

class UserRepository {
    public function save(String $email, String $password = null, String $name, $photo = null, $userEmail = false)
    {
        $user = new User;
        if ($userEmail) {
            $user = $this->get($userEmail);
        }
        $user->email = $email;
        $user->name = $name;
        if (!empty($password)) {
            $user->password = $password;
        }
        $user->photo = $photo;

        return $user->save();
    }

    public function get(String $email = null)
    {
        $user = new User;
        if(!empty($email)) {
           return user::where('email', $email)->first();
        }

        return user::all();
    }

    public function delete(String $email)
    {
        $user = $this->get($email);
        return $user->delete();
    }
}
