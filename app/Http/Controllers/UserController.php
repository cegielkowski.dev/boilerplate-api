<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Business\UserBusiness;
use Illuminate\Http\Response;

class UserController extends Controller
{
    /**
     * Create a new UserController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['create']]);
    }

    /**
     * Create a user.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function create()
    {
        try {
            $userBusiness = new UserBusiness();
            $response = $userBusiness->create(
                request('email'),
                request('password'),
                request('name'),
                request('photo')
            );

            return $response;
        } catch(Exception $e) {
            Log::error($e->getMessage());
        }

        return response(null, 500);
    }

    /**
     * Update the user.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update()
    {
        try {
            $userBusiness = new UserBusiness();
            $user = request('user') ?? auth()->user()->email;
            $response = $userBusiness->update(
                $user,
                request('email'),
                request('password'),
                request('name'),
                request('photo')
            );

            return $response;
        } catch(Exception $e) {
            Log::error($e->getMessage());
        }

        return response(null, 500);
    }

    /**
     * Get one or all users.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function get()
    {
        $userBusiness = new UserBusiness;
        return $userBusiness->get(request('email'));
    }

    /**
     * Delete a user.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete()
    {
        $userBusiness = new UserBusiness;
        return $userBusiness->delete(request('email'));    
    }

}
